resource "google_compute_forwarding_rule" "forwarder" {
  name       = "forwarder-${var.service}"
  ip_address = "${var.ip_address}"
  target     = "${google_compute_target_http_proxy.proxy.self_link}"
  port_range = "${var.port_range}"
}

