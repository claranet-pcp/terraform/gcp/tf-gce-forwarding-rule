output "forwarder_link" {
  value = "${google_compute_forwarding_rule.forwarder.self_link}"
}
output "ip_address" {
  value = "${google_compute_forwarding_rule.forwarder.ip_address}"
}
